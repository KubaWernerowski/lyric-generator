import tweepy
from credentials import CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET


class Account:

    def __init__(self) -> None:
        auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)

        self.api = tweepy.API(auth)

    def update_status(self, tweet_text: str):
        """
        :param tweet_text: markov chain generated lyrics
        :return: Status Objecy
        """
        self.api.update_status(tweet_text)