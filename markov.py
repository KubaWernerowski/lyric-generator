from typing import List, TextIO
import random

class MarkovChain:
    #TODO: Upgrade to Additive Markov Chain
    def __init__(self, text: str) -> None:
        """
        :param text: text input
        """
        self.states = dict()
        all_tokens = self.process_text(text)
        self.train(all_tokens)

    def process_text(self, text: str) -> List:
        """
        :param text: .txt file
        :return: List of all words, separated by spaces.
        """
        replace_chars = ("\n", "(", ")", "\t", ",")
        corpus = ""

        with open(text, 'r', encoding='utf-8-sig') as f:
            for line in f:
                if line[0] == "[":
                    continue
                for char in replace_chars:
                    if char == "\n":
                        line = line.replace(char, " ")
                    else:
                        line = line.replace(char, "")
                corpus = corpus + " " + line.lower()

        return corpus.split()

    def train(self, tokens: List) -> None:
        """
        :param tokens: List of all words. Duplicates included.
        :return: None. Updates self.states.
        """
        for a, b in [(tokens[i], tokens[i+1]) for i in range(len(tokens)-1)]:
            if a not in self.states:
                self.states[a] = {
                    "next_words": [b]
                }
            else:
                self.states[a]["next_words"].append(b)

    def generate(self, max_length: int = 280) -> str:
        """
        :param length: maximum character length of the generated text.
        :return: string of generated text with a random start word.
        """
        start = random.choice(list(self.states.keys()))
        current_state = start
        generated_text = start.capitalize() + " "
        word_count = 1

        while len(generated_text) <= max_length:
            if word_count % 10 == 0:
                generated_text += "\n"

            possible_words = self.states[current_state]["next_words"]
            current_state = random.choice(possible_words)
            # Try to pick new state that will fit with 10 random picks if it doesn't work.
            if len(current_state) + len(generated_text) + 1 > max_length:
                for _ in range(10):
                    possible_words = self.states[current_state]["next_words"]
                    current_state = random.choice(possible_words)
                    if len(current_state) + len(generated_text) <= max_length:
                        generated_text += current_state + " "
                # At this point, our text is likely within 5 characters of the max length, we can just return it.
                return generated_text

            else:
                generated_text +=  current_state + " "
                word_count += 1