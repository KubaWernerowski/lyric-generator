from markov import MarkovChain
from twitter import Account

if __name__ == "__main__":
    account = Account()
    markov_chain = MarkovChain("all_lyrics.txt")

    account.update_status(markov_chain.generate())


